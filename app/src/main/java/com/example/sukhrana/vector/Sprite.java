package com.example.sukhrana.vector;

import android.content.Context;
import android.graphics.Rect;

public class Sprite {
    int xPosition;
    int yPosition;
    int width;
    Rect hitbox;

    public  Sprite(Context context, int x, int y, int w){
        this.xPosition = x;
        this.yPosition = y;
        this.width = w;

        this.hitbox = new Rect(this.xPosition,this.yPosition,this.xPosition+ this.width,this.yPosition+this.width);
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
